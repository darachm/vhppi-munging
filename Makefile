
#
#
# What are viruses? No seriously, gimme the taxids
#
#

### Here's for all viruses
#data/ncbi_new_taxdump.tar.gz : 
#	curl https://ftp.ncbi.nlm.nih.gov/pub/taxonomy/new_taxdump/new_taxdump.tar.gz > $@
#tmp/ncbi_new_taxdump : data/ncbi_new_taxdump.tar.gz 
#	mkdir -p $@ 
#	tar -C $@ -xzf $< 
#tmp/viral_taxids.txt : tmp/ncbi_new_taxdump/taxidlineage.dmp
#	grep "	10239 " $(word 1,$^) | cut -f 1 | sort | uniq \
#		> $@

# But these are all viruses, not just human ones, for that we'll use a table 
# manually downloaded as a CSV from NCBI Virus at https://www.ncbi.nlm.nih.gov/labs/virus/vssi/#/virus?VirusLineage_ss=Viruses,%20taxid:10239&HostLineage_ss=humans,%20taxid:9605&SeqType_s=Nucleotide
# 
tmp/human_viral_taxids.txt : ncbi_api_key data/ncbi_allHumanVirusRefSeq_201015.csv
	export NCBI_API_KEY=$$(cat $(word 1,$^)) && \
		tail -n+2 $(word 2,$^) | cut -d, -f 1 | sort | uniq \
			| parallel --delay 1 --jobs 5 '~/edirect/esearch -db assembly -query "{}" | ~/edirect/elink -target taxonomy | ~/edirect/efetch ' \
			| pv -pterbac --name retrieve \
			| sort | uniq \
			> $@ 2> $@.err

#
#
# All viral proteins
#
#

# these headers current?
# tax_id  Org_name        GeneID  CurrentID       Status  Symbol  Aliases description     other_designations      map_location    chromosome      genomic_nucleotide_accession.version    start_position_on_the_genomic_accession end_position_on_the_genomic_accession orientation     exon_count      OMIM
tmp/human_viral_genes.tsv : ncbi_api_key tmp/human_viral_taxids.txt 
	export NCBI_API_KEY=$$(cat $(word 1,$^)) && \
		cat $(word 2,$^) \
			| parallel --delay 1 --jobs 5 '~/edirect/elink -db taxonomy -target gene -id "{}" |  efetch -format tabular | tail -n+2' \
			| grep -v "^tax_id	" \
			| sort | uniq \
			> $@ 2> $@.err

# Getting IPGs - need to filter out SARS2 because it'll crash it
# geneID \t ipg \t proteinID 
tmp/human_viral_geneID_ipgs_notSARS2.tsv : ncbi_api_key tmp/human_viral_genes.tsv
	export NCBI_API_KEY=$$(cat $(word 1,$^)) && \
		cat $(word 2,$^) \
			| grep -v "Severe acute respiratory syndrome coronavirus 2" \
			| cut -f 3 \
			| parallel --delay 1 --jobs 5 'echo {} 1>&2 ; ~/edirect/elink -db gene -id {} -target protein | efetch -format ipg | tail -n+2 |  sed "s/^/{}\t/" ' \
				2> $@.err \
			| grep -v "	tax_id	" \
			| pv -betlapc --name retrieve \
			| mawk -F'\t' '{ print $$1"\t"$$2"\t"$$8 }' \
			| sort | uniq \
			| pv -betlapc --name output \
			> $@ 

##### TODO how do I pull down the IPGs for SARS2 proteins without running out of hard disk space?
#			| parallel --delay 1 --jobs 5 'echo {} 1>&2 ; ~/edirect/elink -db gene -id {} -target protein | efetch -format ipg | tail -n+2 | mawk -F"	" "{ print \$$1\"\\t\"\$$7}" | sort | uniq | sed "s/^/{}\t/" ' \
#			> $@ 
#tmp/human_viral_geneID_ipgs_SARS2.tsv : ncbi_api_key tmp/human_viral_genes.tsv
#	export NCBI_API_KEY=$$(cat $(word 1,$^)) && \
#		cat $(word 2,$^) \
#			| grep "Severe acute respiratory syndrome coronavirus 2" \
#			| cut -f 3 \
#			#| parallel --delay 1 --jobs 5 'echo {} 1>&2 ; ~/edirect/elink -db gene -id {} -target protein | efetch -format ipg | tail -n+2 | cut -f 1,7 | sort | uniq | sed "s/^/{}\t/" ' \
#			2> $@.err \
#			| grep -v "	tax_id	" \
#			> $@ 
#
#	#		| sort | uniq \


# SCRAPS:
## Output is list of refseq/genbank/embl accessions for viral genes
#tmp/viral_genes_geneID_acc.tsv : ncbi_api_key tmp/viral_genes.tsv
#	export NCBI_API_KEY=$$(cat $(word 1,$^)) && \
#		export PATH=$${PATH}:~/edirect && \
#		mawk -F'	' '{ print $$3 }' $(word 2,$^) \
#			| sort | uniq \
#			| parallel --delay 1 --jobs 1 '~/edirect/esearch -db gene -query "{}" | ~/edirect/elink -target protein | ~/edirect/efetch -format acc | sed "s/^/{} /"' \
#			> $@ 2> $@.err
## Output is list of grep regexes refseq/genbank/embl to pull viral seq out of nr fasta
#tmp/viral_geneAccFastaRegexes.txt : tmp/viral_genes_geneID_acc.tsv
#	mawk '{ print ">"$$2" " }' $(word 1,$^) | sort | uniq > $@
###### TODO rewrite to automatically retry the errors! twice! then collect all results
## taxid10239.nbr is downloaded from front page of Viral Genomes NCBI, lower left, all genome accessions
## Id      Source  Nucleotide Accession    Start   Stop    Strand  Protein Protein Name    Organism        Strain  Assembly
#tmp/viral_ipgs.tsv : ncbi_api_key tmp/viral_taxids.txt
#	export NCBI_API_KEY=$$(cat $(word 1,$^)) && \
#		cat $(word 1,$^) \
#			| parallel --pipe --block 1000 'tr "\n" "," | sed "s/,$$/\n/" ' \
#			| parallel --delay 1 --jobs 1 '~/edirect/elink -db taxonomy -id {} -target protein | ~/edirect/efetch -format ipg | tail -n+2 | sort | uniq ' \
#			| grep -v "^Id	" \
#			| pv -pterbac \
#			> $@ 2> $@.err
#tmp/viral_taxid_sorted.txt : tmp/viral_genes.tsv
#	tail -n+2 $(word 1,$^) | mawk -F'	' '{ print $$1 }' \
#		| sort | uniq | grep -v "[a-z]" \
#		> $@
#tmp/viral_proteinToIpg.txt : tmp/viral_ipgs.tsv
#	cat $(word 1,$^) | mawk -F'\t' '{ print $$7"\t"$$1 }' \
#		| sed 's/\.[0-9]//' \
#		| sort -t'	' -k 1,1 | uniq | grep -v "[a-z]" \
#		> $@
## making taxid list flanked by tabs for grep
##tmp/viral_taxid_grepTabPadregex.txt : tmp/viral_genes.tsv
## Just filtering out the tax 10239 from unimap
#tmp/viral_unimap.tsv : data/idmapping_selected.tab.gz \
#		tmp/viral_taxid_grepTabPadregex.txt
#	zgrep -Ff $(word 2,$^) $(word 1,$^) | pv -pterbac > $@
	
## Output has GeneID \t UniRef90 \t RefSeq \t EMBL-CDS \t taxid 
#tmp/viral_genes_subset.tsv : tmp/viruses_genes_unimap.tsv tmp/viral_genes.tsv
#	gawk -F'	' '{ print $$1"\t"$$3 }' $(word 2,$^) \
#		| sort -t '	' -k 2,2 \
#		> tmp/$(notdir $(word 2,$^)).cut.sorted
#	cat $(word 1,$^) \
#		| mawk -F'\t' '{ split($$3,a,"; "); for (i in a) { print a[i]"\t"$$9"\t"$$4"\t"$$18; } }' \
#		| sort -t '	' -k 1,1 \
#		| join -t '	' -1 1 -2 2 - tmp/$(notdir $(word 2,$^)).cut.sorted \
#		| sort -t '	' -k 3,4 | uniq \
#		> $@









#
#
# Unimap file prep
#
#

# uniprot id mapping file columns
#1. UniProtKB-AC
#2. UniProtKB-ID
#3. GeneID (EntrezGene)
#4. RefSeq
#5. GI
#6. PDB
#7. GO
#8. UniRef100
#9. UniRef90
#10. UniRef50
#11. UniParc
#12. PIR
#13. NCBI-taxon
#14. MIM
#15. UniGene
#16. PubMed
#17. EMBL
#18. EMBL-CDS
#19. Ensembl
#20. Ensembl_TRS
#21. Ensembl_PRO
#22. Additional PubMed

tmp/human_viral_unimap.regex : tmp/human_viral_taxids.txt
	mawk '{ print "\t"$$0"\t"; print "\t"$$0"; "; print "; "$$0"\t" }' $< \
		| sort | uniq > $@

# These scraps for pulling via IPGs, not taxid
#tmp/human_viral_geneID_ipgs_notSARS2.tsv.sorted : tmp/human_viral_geneID_ipgs_notSARS2.tsv
#	sort -t'	' -k3,3 $(word 1,$^) | uniq > $@
#tmp/human_viral_unimap.regex : tmp/human_viral_geneID_ipgs_notSARS2.tsv.sorted \
#		tmp/human_viral_taxids.txt
#	mawk -F'\t' '{ if ($$3 != "") {print "\t"$$3"\t"; print "; "$$3"\t"; print "\t"$$3"; "}}' $< \
#		| sort | uniq > $@
### Output is 
### genbank \t geneID \t taxid \t the above original unimap table
#tmp/human_viral_unimap.tsv : data/idmapping_selected.tab.gz \
#		tmp/human_viral_genbankAcc_notSARS2.txt \
#		tmp/human_viral_geneID_ipgs_notSARS2.tsv.sorted
#	zcat $(word 1,$^) \
#		| pv -pterbac --name zcat \
#    	| grep -Ff $(word 2,$^) \
#		| mawk -F'\t' '{ split($$18,a,"; "); for (i in a) { print a[i]"\t"$$0; } }' \
#		| pv -pterbac --name mawk \
#		| sort -t '	' -k 1,1 \
#		| uniq \
#		| pv -pterbac --name sortuniq \
#	   	| join -t '	' -1 3 -2 1 $(word 3,$^) - \
#		| pv -pterbac --name join \
#		> $@
### Output is 
### genbank \t geneID \t taxid \t the above original unimap table
#tmp/human_viral_unimap.tsv : data/idmapping_selected.tab.gz \
#		tmp/human_viral_genbankAcc_notSARS2.regex \
#		tmp/human_viral_geneID_ipgs_notSARS2.tsv.sorted
#	zcat $(word 1,$^) \
#		| pv -pterbac --name zcat \
#    	| grep -Ff $(word 2,$^) \
#		| mawk -F'\t' '{ split($$18,a,"; "); for (i in a) { print a[i]"\t"$$0; } }' \
#		| pv -pterbac --name mawk \
#		| sort -t '	' -k 1,1 \
#		| uniq \
#		| pv -pterbac --name sortuniq \
#	   	| join -t '	' -1 3 -2 1 $(word 3,$^) - \
#		| pv -pterbac --name join \
#		> $@


# Downloaded the ID mapping file from https://www.uniprot.org/downloads for UniProt-KB
## Output is 
## genbank \t geneID \t taxid \t the above original unimap table
tmp/human_viral_unimap.tsv : data/idmapping_selected.tab.gz tmp/human_viral_unimap.regex
	zgrep $(word 1,$^) -Ff $(word 2,$^) > $@

#
#
# BLAST
#
#

# Output is list of grep regexes refseq/genbank/embl to pull viral seq out of nr fasta
tmp/human_viral_proteinRegexes.txt : tmp/human_viral_unimap.tsv
	mawk -F'\t' '{ split($$4,a,"; "); for (i in a) print ">"a[i]" " }' $(word 1,$^) | sort | uniq > $@
	mawk -F'\t' '{ split($$18,a,"; "); for (i in a) print ">"a[i]" " }' $(word 1,$^) | sort | uniq >> $@

# Selecting all fasta files that match those identifiers
/mnt/scratch/databases/ncbi_nr/nr_viral.fasta : scripts/viral_subset.sh \
		/mnt/scratch/databases/ncbi_nr/nr.gz \
		tmp/human_viral_proteinRegexes.txt 
	mkdir -p $(dir $@)
	$^ > $@

/mnt/scratch/databases/ncbi_nr/nr_viral.fasta.psq : ~/.singularity/cache/ncbi-blast.simg \
		/mnt/scratch/databases/ncbi_nr/nr_viral.fasta
	mkdir -p tmp/db
	export SINGULARITYENV_BLASTDB=$(shell pwd)/tmp/db && \
		singularity exec --bind $(dir $(word 2,$^)):$(shell pwd)/tmp/db $(word 1,$^) \
			makeblastdb -in tmp/db/$(notdir $(word 2,$^)) -dbtype prot -parse_seqids -title "nr_viral"


#
#
# Taxonomy of DNASU file, doing it via blast
#
#

# Pulling ORF sequences out of the CSV, into FASTA
# that first mawk is from https://unix.stackexchange.com/questions/48672/remove-comma-between-the-quotes-only-in-a-comma-delimited-file#48675
# Output is FASTA, IDs are DNASUid, gsub for getting rid of quoted commas
tmp/dnasu_orfs.fasta : data/Viral_gene_clones_DNASU_2020_05_semifiltered.csv 
	tail -n+2 $(word 1,$^) \
		| mawk -F'"' -v OFS='' '{ for (i=2; i<=NF; i+=2) gsub(",","",$$i) } 1' \
		| mawk -F',' '{ print ">"$$1"\n"$$17}' > $@

# Blasting these against the viral protein database
# std blast 10 output is 
# ' qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore '
# So this output is
# ' qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore qlen slen nident '
tmp/dnasu_orfs.blasted : ~/.singularity/cache/ncbi-blast.simg \
		tmp/dnasu_orfs.fasta /mnt/scratch/databases/ncbi_nr/nr_viral.fasta.psq
	mkdir -p tmp/db
	export SINGULARITYENV_BLASTDB=$(shell pwd)/tmp/db && \
		singularity exec --bind $(dir $(word 3,$^)):$(shell pwd)/tmp/db $(word 1,$^) \
    		blastx -query $(word 2,$^) -db $(notdir $(basename $(word 3,$^))) \
    			-num_threads 16 -outfmt "10 std qlen slen nident " -out $@

# This filters out the single best hit, as judged by...?
tmp/dnasu_orfs.best_hit : scripts/pick_blast_best.py tmp/dnasu_orfs.blasted
	python3 $^ $@

# Map the best hit accession to uniref90 id, associated DNASUid with UniRef90, taxid
tmp/dnasu_uniref.tsv : tmp/dnasu_orfs.best_hit tmp/human_viral_unimap.tsv 
	cat $(word 1,$^) \
		| mawk -F, '{ print $$2"	"$$1"	"$$3 }' \
		| sort -t'	' -k1,1 \
		> $(word 1,$^).cut.sorted
	cat $(word 2,$^) \
		| mawk -F'	' '{ split($$7,a,"; "); for (i in a) { print a[i]"\t"$$8"\t"$$9 }}' \
		| sort | uniq \
		| sort -t'	' -k1,1 \
		| join -t'	' -1 1 -2 1 - $(word 1,$^).cut.sorted \
		> $@
	cat $(word 2,$^) \
		| mawk -F'	' '{ split($$21,a,"; "); for (i in a) { print a[i]"\t"$$9"\t"$$9 }}' \
		| sort | uniq \
		| sort -t'	' -k1,1 \
		| join -t'	' -1 1 -2 1 - $(word 1,$^).cut.sorted \
		>> $@

#
#
# Mapping taxid to families etc
#
#


#
#
# VirHostNet
#
#

# This generates a file where the taxonomy id has been prefixed by "taxid:",
# for purposes of matching/grep'ing
tmp/viral_taxid_prefixRegex.txt : tmp/human_viral_taxids.txt
	mawk '{ print "taxid:"$$0"\t" }' $(word 1,$^) | sort > $@

# This is what you get from downloading the VirHostNet data, it seems to be
# a dump of their database... with no metadata or headers.
# data/virhostnet_dump_200925.tsv
# pulled from http://virhostnet.prabi.fr:9090/psicquic/webservices/current/search/query/*

# Here filter the ones of acceptable evidence from VirHostNet, pull out the
# real viruses, and also ones that are interacting with humans
# output is human uniprotkb with prefix \t viral identifier with prefix \t viral bare identifier
tmp/virhostnet_acceptable.txt : data/virhostnet_dump_200925.tsv \
		data/virhostnet_acceptable_evidence_200925.txt  \
		tmp/viral_taxid_prefixRegex.txt
	grep -Ff $(word 2,$^) $(word 1,$^) \
		| grep "taxid:9606	" \
    	| grep -Ff $(word 3,$^) \
    	| mawk -F'\t' '{if ($$10 == "taxid:9606") { \
					print $$1"\t"$$2 \
				} else { \
					print $$2"\t"$$1} }' \
		| sort -k 2,2 | uniq \
		| perl -ne 's/:(\S*?)$$/:\1\t\1/;print;' \
		| sort -k3,3 \
    	> $@

##tmp/virhostnet_viralIDs.txt: tmp/virhostnet_acceptable_human_first.txt
#	#cut -f 3 $< > $@
#	#cut -f 3 $< | mawk '{print ""$$0""}' >> $@
# here, I had thought that I shoudl pad these grep patterns with tabs, but no
#tmp/virhostnet_unimap_indicies_tabpadded.txt: 
#	cut -f 2 $< | sed 's/.*://' | grep "uniprotkb:" \
#		| mawk '{print $$0""}' > $@
#tmp/virhostnet_unimap_subset.tsv: \
#		tmp/virhostnet_viralIDs.txt
#	zgrep -Ff $(word 2,$^) $(word 1,$^) | pv -pterbac > $@
	#	tmp/viruses_genes_unimap.tsv \
#tmp/virhostnet_uniref90.tsv: tmp/virhostnet_unimap_subset.tsv \
#		tmp/virhostnet_acceptable_human_first.txt




# I think it's
# uniprot viral \t taxid \t uniref 100 \t uniref90 \t human uniprot \t source
tmp/virhostnet_uniref.tsv : tmp/human_viral_unimap.tsv \
		tmp/virhostnet_acceptable.txt 
	mawk -F '\t' '{ split($$1,a,"; "); for (i in a) print a[i]"\t"$$8"\t"$$9"\t"$$13 }' $(word 1,$^) \
		| sort -t '	' -k1,1 \
		| join -t '	' -1 1 -2 3 - $(word 2,$^) \
		| mawk -F'\t' '{ print $$1"\t"$$4"\t"$$2"\t"$$3"\t"$$5"\tvirhost"}' \
		> $@.uniprotkb-ac
	mawk -F '\t' '{ split($$4,a,"; "); for (i in a) print a[i]"\t"$$8"\t"$$9"\t"$$13 }' $(word 1,$^) \
		| sed 's/\.[0-9]	/\t/' \
		| sort -t '	' -k1,1 \
		| join -t '	' -1 1 -2 3 - $(word 2,$^) \
		| mawk -F'\t' '{ print $$1"\t"$$4"\t"$$2"\t"$$3"\t"$$5"\tvirhost"}' \
		> $@.refseq
	mawk -F '\t' '{ split($$18,a,"; "); for (i in a) print a[i]"\t"$$8"\t"$$9"\t"$$13 }' $(word 1,$^) \
		| sort -t '	' -k1,1 \
		| join -t '	' -1 1 -2 3 - $(word 2,$^) \
		| mawk -F'\t' '{ print $$1"\t"$$4"\t"$$2"\t"$$3"\t"$$5"\tvirhost"}' \
		> $@.embl-cds
	cat $@.uniprotkb-ac $@.refseq $@.embl-cds \
		| sed 's/\t\S\+:/\t/g' > $@

# human uniprot good enough?



#
#
# Viral STRING
#
#

# protein.links.detailed.v10.5.txt.gz 

# This generates a file where the taxonomy ids are turned into patterns for
# grep to use, for pulling taxid numbers out of fore or aft of string database
tmp/viral_taxid_stringRegex.txt : tmp/viral_taxids.txt
	mawk '{ print "^"$$0"." }' $(word 1,$^) > $@
	mawk '{ print " "$$0"." }' $(word 1,$^) >> $@

# STRING database files were pulled from http://viruses.string-db.org/cgi/download.pl 
# STRING header, so column order, is
# protein1 protein2 neighborhood fusion cooccurence coexpression experimental database textmining combined_score
tmp/string_viral.tab : data/protein.links.detailed.v10.5.txt.gz \
		tmp/viral_taxid_stringRegex.txt
	zcat $(word 1,$^) | pv -pterbac  \
		| parallel --pipe --block 100M grep -F -f $(word 2,$^) \
		| pv -pterbac \
		> $@

# except for the aliases file for version 11, which comes from https://string-db.org/cgi/download.pl
tmp/string_enspToUniprot.tsv : data/9606.protein.aliases.v11.0.txt.gz 
	zcat $(word 1,$^) | grep "	Ensembl_UniProt$$" \
		| cut -d'	' -f1-2 \
		| sort -t'	' -k1,1 \
		> $@

# This ones from viruses.string-db.org downloads
tmp/string_viralToUniprot.tsv : data/protein.aliases.v10.5.txt.gz
	zcat $(word 1,$^) \
		| cut -d'	' -f1-2 \
		| sort -t'	' -k1,1 \
		> $@
		#| grep "	UniProtKB-EI$$" \

# So now this is human id, viral id, and experimental evidence for it
# output is
# viral taxid \t viral id in string \t score \t human uniprot \t mix of viral identifiers!
tmp/string_viral_acceptable.tsv : tmp/string_viral.tab \
		tmp/string_enspToUniprot.tsv \
		tmp/string_viralToUniprot.tsv
	cat $(word 1,$^) | pv -pterbac  \
		| gawk '{ if( $$7 != "0" ) { print $$0 } }' \
		| mawk '{ if (match($$1,/^9606\./) != 0) { print $$1"\t"$$2"\t"$$7 } \
			else if (match($$2,/^9606\./) != 0) { print $$2"\t"$$1"\t"$$7 } }' \
		| sort -t'	' -k 1,1 \
		| join -t'	' -j 1 - $(word 2,$^) \
		| cut -d'	' -f2-4 \
		| sort -t'	' -k 1,1 \
		| join -t'	' -j 1 - $(word 3,$^) \
		| sed 's/\./\t/' \
		| sort -t '	' -k5,5 \
		| pv -pterbac \
		> $@

# not mapping viral to uniprot! these parts do htat
		# then sed
	#	| mawk -F'\t' '{ print $$1"\t"$$5"\t"$$3"\t"$$4}' \


# Output  is
# uniprot viral \t taxid \t uniref 100 \t uniref90 \t human uniprot \t source
tmp/string_viral_uniref_human_uniprot.tsv : tmp/human_viral_unimap.tsv \
		tmp/string_viral_acceptable.tsv
	sort -t'	' -k 1,1 $(word 1,$^) \
		| join -t'	' -1 5 -2 1 $(word 2,$^) - \
		| mawk -F'\t' '{ print $$1"\t"$$2"\t"$$12"\t"$$13"\t"$$5"\tstring"}' \
		| sort | uniq \
		> $@



#		| sort -t '	' -k 1,1 \
#		| join -t '	' -1 1 -2 5 - $(word 2,$^) \
#		| mawk -F'\t' '{ print $$1"\t"$$7"\t"$$4"\t"$$5"\t"$$10"\tstring"}' \
#		> $@

		#| head -n 100000 \

# 2-3,5,9,10,19 should be : two uniprots, refseq, uniref100 and 90, embl-cds

.PHONY: comparison
comparison : tmp/virhostnet_uniref.tsv tmp/string_viral_uniref_human_uniprot.tsv tmp/dnasu_uniref.tsv





# Trash

#data/viral.1.protein.faa.gz : 
#	curl https://ftp.ncbi.nlm.nih.gov/refseq/release/viral/viral.1.protein.faa.gz > $@
#data/viral.2.protein.faa.gz : 
#	curl https://ftp.ncbi.nlm.nih.gov/refseq/release/viral/viral.2.protein.faa.gz > $@
#
#tmp/ncbi_viral_protein.fasta : \
#		data/viral.1.protein.faa.gz data/viral.2.protein.faa.gz
#	zcat $^ | sed 's/ .*$$//' > $@

#tmp/ncbi_viral_protein.fasta.ids : tmp/ncbi_viral_protein.fasta
#	grep "^>" $< | sed 's/^>//' | sort > $@
#
#data/ncbi_prot.accession2taxid.gz : 
#	curl https://ftp.ncbi.nlm.nih.gov/pub/taxonomy/accession2taxid/prot.accession2taxid.gz > $@
#
#tmp/ncbi_prot.accession2taxid_viral.tsv : data/ncbi_prot.accession2taxid.gz \
#		tmp/ncbi_viral_protein.fasta.ids
#	zcat $(word 1,$^) | pv -pterbac | grep -Ff $(word 2,$^) | pv -pterbac > $@ 
#
#tmp/ncbi_viral_taxidmap.csv : tmp/ncbi_prot.accession2taxid_viral.tsv 
#	mawk -F'	' '{print $$2","$$3}' $^ | sort -t, -k1,1 > $@


