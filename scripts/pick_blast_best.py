#!/usr/bin/env python3

import pandas as pd
import numpy as np

import sys

csv_path = sys.argv[1]
out_path = sys.argv[2]

pd.read_csv(csv_path,header=None).groupby(0) \
    .apply(lambda x: x.sort_values(by=2, ascending=True).iloc[0,:]) \
    .to_csv(out_path,header=False,index=False)
