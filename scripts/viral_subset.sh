
>&2 echo "Searching through fasta.gz $1, looking for identifiers matching $2, spitting out FASTA to STDOUT"

zcat $1 | pv -pterbac \
    | cut -d' ' -f 1 \
    | parallel --pipe --block 10M -k "tr '\n' ' ' | sed 's/>/\n>/g' " \
    | pv -pterbac \
    | parallel --pipe --block 10M "grep -Ff $2" \
    | sed 's/ /\n/' | tr -d ' ' 
